'use strict'

const printer = require('./printer_module.js').printer;

var MyDefaultPrinter = (obj) => {
  let result = printer(obj);
  return result;
}
//let result = MyDefaultPrinter('hola');
//console.log(result);

module.exports.MyDefaultPrinter = MyDefaultPrinter;
